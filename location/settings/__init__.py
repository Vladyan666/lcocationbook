from .settings_global import *
try:
    from .settings_local import *
except ImportError as e:
    print(e)