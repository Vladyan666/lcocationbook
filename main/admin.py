from django.contrib import admin
from django.utils.safestring import mark_safe
from .forms import LocationForm

from .models import *

page_fields = [
    ("Настройки страницы", {'fields': ['alias']}),
    ("SEO информация", {'fields': ['seo_h1', 'seo_title', 'seo_description', 'seo_keywords', 'content']})
]

class TextPageAdmin(admin.ModelAdmin):
#    fieldsets = [(u'Основные', {'fields': ['name', 'og_title', 'og_description', 'og_type_pb_time', 'og_type_author',]}), ] + page_fields
    fieldsets = [('Основные', {'fields': ['name',]})] + page_fields
    list_display = ['name']

    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {"alias": ("name",)}

#
admin.site.register(TextPage, TextPageAdmin)

class LocationPhotoInline(admin.TabularInline):
    model = LocationPhoto
    extra = 3
    readonly_fields = ["preview"]
    
    def preview(self, obj):
        return mark_safe('<img src="{url}" width="{width}" height={height} />'.format(
            url = obj.image.url,
            width=200,
            height=150,
        ))

class LocationAdmin(admin.ModelAdmin):
    form = LocationForm
    fieldsets = [(u'Основные', {'fields': ['name', 'number', 'to_main', 'loc_descr', 'area', 'loc_type', 'room', 'style', 'addres', 'address_custom', ]}), ] + page_fields
    list_display = ['pk', 'name', 'loc_type', 'style', 'to_main']
    inlines = [LocationPhotoInline]
    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {"alias": ("name",)}

#
admin.site.register(Location, LocationAdmin)

class PartnersAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные', {'fields': ['name', 'image', 'url', ]}), ]
    list_display = ['name', 'url']

    # def get_prepopulated_fields(self, request, obj=None):
    #     # can't use `prepopulated_fields = ..` because it breaks the admin validation
    #     # for translated fields. This is the official django-parler workaround.
    #     return {"alias": ("name",)}

#
admin.site.register(Partners, PartnersAdmin)

class BlogPageAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные', {'fields': ['name', 'image', ]}), ] + page_fields
    list_display = ['name']

    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {"alias": ("name",)}

#
admin.site.register(BlogPage, BlogPageAdmin)

#коМАНДА
admin.site.register(Member)

#Услуга
admin.site.register(Service)

#Популярное
admin.site.register(Popular)

#Текстовая переменная
admin.site.register(TextVariable)