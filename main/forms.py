from django import forms
from .models import ROOM_TYPE, Location

class LocationForm(forms.ModelForm):
    room = forms.MultipleChoiceField(choices=ROOM_TYPE, widget=forms.SelectMultiple)
    class Meta:
        model = Location
        fields = '__all__'


class ContactForm(forms.Form):
    name = forms.CharField(max_length=100, widget=forms.TextInput(
        {'placeholder': 'Ваше имя', 'required': 'True', 'type': 'text', 'name': 'name', 'class': 'main_form__input'}))
    phone = forms.CharField(max_length=100, widget=forms.TextInput(
        {'placeholder': 'Ваше телефон', 'required': 'True', 'type': 'text', 'name': 'name', 'class': 'main_form__input'}))
    email = forms.CharField(max_length=100, widget=forms.TextInput(
        {'placeholder': 'Ваш Email', 'type': 'text', 'name': 'email', 'class': 'main_form__input'}), required=False)
    project = forms.CharField(max_length=100, widget=forms.TextInput(
        {'placeholder': 'Название проекта', 'type': 'text', 'name': 'email', 'class': 'main_form__input'}), required=False)
    days = forms.CharField(max_length=100, widget=forms.TextInput(
        {'placeholder': 'Кол-во съемочных дней', 'type': 'text', 'name': 'email', 'class': 'main_form__input'}), required=False)
    loc_type = forms.CharField(max_length=100, widget=forms.TextInput(
        {'placeholder': 'Тип локации', 'type': 'text', 'name': 'email', 'class': 'main_form__input'}), required=False)
    file = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': False}), required=False)