# Generated by Django 2.0 on 2019-05-03 13:21

import ckeditor.fields
import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Partners',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('alias', models.SlugField(max_length=200, verbose_name='Url')),
                ('og_title', models.CharField(blank=True, max_length=200, null=True, verbose_name='OG Title')),
                ('og_description', models.TextField(blank=True, max_length=2000, null=True, verbose_name='OG Description')),
                ('og_type', models.CharField(blank=True, max_length=200, null=True, verbose_name='OG Type')),
                ('og_type_pb_time', models.DateField(default=datetime.datetime(2019, 5, 3, 13, 21, 58, 838624), verbose_name='Время публикации')),
                ('og_type_author', models.CharField(blank=True, max_length=200, null=True, verbose_name='OG author')),
                ('seo_h1', models.CharField(blank=True, max_length=200, null=True, verbose_name='H1')),
                ('seo_title', models.CharField(blank=True, max_length=200, null=True, verbose_name='Title')),
                ('seo_description', models.CharField(blank=True, max_length=500, null=True, verbose_name='Description')),
                ('seo_keywords', models.CharField(blank=True, max_length=500, null=True, verbose_name='Keywords')),
                ('menutitle', models.CharField(blank=True, help_text='Не обязательно', max_length=200, null=True, verbose_name='Название в меню')),
                ('content', ckeditor.fields.RichTextField(blank=True, null=True)),
                ('image', models.ImageField(upload_to='partners', verbose_name='Фото')),
                ('name', models.CharField(blank=True, max_length=200, null=True, verbose_name='Название')),
            ],
            options={
                'verbose_name_plural': 'Партнеры',
                'verbose_name': 'Партнер',
            },
        ),
        migrations.AlterModelOptions(
            name='location',
            options={'verbose_name': 'Локацию', 'verbose_name_plural': 'Локации'},
        ),
        migrations.AlterField(
            model_name='blogpage',
            name='menutitle',
            field=models.CharField(blank=True, help_text='Не обязательно', max_length=200, null=True, verbose_name='Название в меню'),
        ),
        migrations.AlterField(
            model_name='blogpage',
            name='og_type_pb_time',
            field=models.DateField(default=datetime.datetime(2019, 5, 3, 13, 21, 58, 838624), verbose_name='Время публикации'),
        ),
        migrations.AlterField(
            model_name='location',
            name='menutitle',
            field=models.CharField(blank=True, help_text='Не обязательно', max_length=200, null=True, verbose_name='Название в меню'),
        ),
        migrations.AlterField(
            model_name='location',
            name='og_type_pb_time',
            field=models.DateField(default=datetime.datetime(2019, 5, 3, 13, 21, 58, 838624), verbose_name='Время публикации'),
        ),
        migrations.AlterField(
            model_name='textpage',
            name='menutitle',
            field=models.CharField(blank=True, help_text='Не обязательно', max_length=200, null=True, verbose_name='Название в меню'),
        ),
        migrations.AlterField(
            model_name='textpage',
            name='og_type_pb_time',
            field=models.DateField(default=datetime.datetime(2019, 5, 3, 13, 21, 58, 838624), verbose_name='Время публикации'),
        ),
    ]
