# Generated by Django 2.0 on 2019-06-23 21:14

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0013_auto_20190623_1932'),
    ]

    operations = [
        migrations.AddField(
            model_name='location',
            name='area',
            field=models.CharField(blank=True, max_length=200, null=True, verbose_name='Площадь'),
        ),
        migrations.AlterField(
            model_name='blogpage',
            name='og_type_pb_time',
            field=models.DateField(default=datetime.datetime(2019, 6, 23, 21, 14, 17, 344052), verbose_name='Время публикации'),
        ),
        migrations.AlterField(
            model_name='location',
            name='og_type_pb_time',
            field=models.DateField(default=datetime.datetime(2019, 6, 23, 21, 14, 17, 344052), verbose_name='Время публикации'),
        ),
        migrations.AlterField(
            model_name='partners',
            name='og_type_pb_time',
            field=models.DateField(default=datetime.datetime(2019, 6, 23, 21, 14, 17, 344052), verbose_name='Время публикации'),
        ),
        migrations.AlterField(
            model_name='textpage',
            name='og_type_pb_time',
            field=models.DateField(default=datetime.datetime(2019, 6, 23, 21, 14, 17, 344052), verbose_name='Время публикации'),
        ),
    ]
