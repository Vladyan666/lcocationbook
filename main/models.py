from __future__ import unicode_literals
from django.db import models
from django.conf import settings as django_settings
from ckeditor.fields import RichTextField
from django.contrib.postgres.fields import JSONField
from django.urls import reverse
from datetime import datetime
import uuid
import os
import shutil
import re

from multiselectfield import MultiSelectField

LOCATION_TYPE = (
    (1, 'квартиры'),
    (2, 'дома'),
    (3, 'пентхаусы'),
    (4, 'дома отдыха'),
    (5, 'гостиницы и отели'),
    (6, 'дачи'),
    (7, 'усадьбы'),
    (8, 'мансарды'),
    (9, 'магазины'),
    (10, 'больница и клиники'),
    (11, 'школы'),
    (12, 'детские сады'),
    (13, 'салоны красоты'),
    (14, 'клубы и бары'),
    (15, 'рестораны и кафе'),
    (16, 'заброшенные здания'),
    (17, 'институты и академии'),
    (18, 'лофты'),
    (19, 'гаражи'),
    (20, 'автомойки и сервисы'),
    (21, 'подъезды'),
    (22, 'дворцы культуры'),
    (23, 'склады'),
    (24, 'павильоны'),
    (25, 'аэропорты и вокзалы'),
    (26, 'офисы'),
    (27, 'мастерские'),
    (28, 'почта'),
    (29, 'спорт'),
    (30, 'выставочные помещения'),
    (31, 'парковки'),
    (32, 'крыши и чердаки'),
    (33, 'подвалы'),
    (34, 'отделения полиции'),
    (35, 'храмы'),
    (36, 'морги'),
    (37, 'лес'),
    (38, 'поле'),
    (39, 'двор'),
    (40, 'улица'),
    (41, 'водоем'),
    (42, 'парк'),
    (43, 'кладбище'),
    (44, 'яхты'),
    (45, 'самолеты'),
    (46, 'вертолеты'),
    (47, 'автомобили')
)

ROOM_TYPE = (
    (1, 'кухня'),
    (2, 'гостиная'),
    (3, 'кабинет'),
    (4, 'библиотека'),
    (5, 'детская'),
    (6, 'ванная'),
    (7, 'спальня'),
    (8, 'бассейн'),
    (9, 'сауна'),
    (10, 'гараж'),
    (11, 'веранда'),
    (12, 'новостройка'),
    (13, 'другое')
)

STYLE_TYPE = (
    (1, 'современный'),
    (2, 'советский'),
    (3, 'классический'),
    (4, 'хай-тек'),
    (5, 'деревенский'),
    (6, 'исторический'),
    (7, 'скандинавский'),
    (8, 'лофт'),
    (9, 'модерн'),
    (10, 'эко'),
    (11, 'минимализм'),
    (12, 'эклектика'),
    (13, 'азиатский'),
    (14, 'готика')
)

LOCATION_PLACE = (
    (0, 'Не указано'),
    (1, 'ЮАО'),
    (2, 'ЮЗАО'),
    (3, 'ЗАО'),
    (4, 'СЗАО'),
    (5, 'САО'),
    (6, 'СВАО'),
    (7, 'ВАО'),
    (8, 'ЮВАО'),
    (9, 'ЦАО'),
    (10, 'НАО'),
    (11, 'ТАО'),
    (12, 'Московская область'),
    (13, 'Другое'),
    (14, 'Киевское шоссе'),
    (15, 'Калужское шоссе'),
    (16, 'Новорижское шоссе'),
    (17, 'Симферопольское шоссе'),
    (18, 'Рублево-Успенское шоссе'),
    (19, 'Минское шоссе')
)


class Page(models.Model):
    class Meta:
        abstract = True

    alias = models.SlugField(max_length=200, verbose_name=u"Url")
    og_title = models.CharField(max_length=200, verbose_name="OG Title", null=True, blank=True)
    og_description = models.TextField(max_length=2000, verbose_name="OG Description", null=True, blank=True)
    og_type = models.CharField(max_length=200, verbose_name="OG Type", null=True, blank=True)
    og_type_pb_time = models.DateField(default=datetime.now(), verbose_name=u"Время публикации")
    og_type_author = models.CharField(max_length=200, verbose_name="OG author", null=True, blank=True)
    seo_h1=models.CharField(max_length=200, verbose_name="H1", null=True, blank=True)
    seo_title=models.CharField(max_length=200, verbose_name="Title", null=True, blank=True)
    seo_description=models.CharField(max_length=500, verbose_name="Description", null=True, blank=True)
    seo_keywords=models.CharField(max_length=500, verbose_name="Keywords", null=True, blank=True)
    menutitle=models.CharField(max_length=200, verbose_name=u"Название в меню", null=True, blank=True, help_text='Не обязательно')
    content = RichTextField(config_name='default', null=True, blank=True)
    
class TextPage(Page):
    class Meta:
        verbose_name = u"Текстовая страница"
        verbose_name_plural = u"Страницы"
        
    name = models.CharField(max_length=200, verbose_name=u"Название")
    alias = models.SlugField(max_length=200, verbose_name=u"Url", unique=True)

    def get_absolute_url(self):
        if self.alias == 'index':
            return '/'
        else:
            return reverse('main:textpage', args=[self.alias])
    
    def __str__(self):
        return self.name
        
class BlogPage(Page):
    class Meta:
        verbose_name = u"Страница блога"
        verbose_name_plural = u"Блог"
    
    name=models.CharField(max_length=200, verbose_name=u"Название")
    datetime = models.DateTimeField(verbose_name=u"Дата публикации", auto_now_add=True, editable=False)
    image = models.ImageField(verbose_name=u"Првеью Блога", null=True, blank=True)
    
    def __str__(self):
        return self.name
        
class Location(Page):
    class Meta:
        verbose_name = u"Локацию"
        verbose_name_plural = u"Локации"
        
    name = models.CharField(max_length=200, verbose_name=u"Название")
    area = models.CharField(max_length=200, verbose_name=u"Площадь", null=True, blank=True)
    number = models.IntegerField(verbose_name=u"Номер локации", null=True, blank=True)
    to_main = models.BooleanField(default=False, verbose_name=u"Актуальная локация")
    loc_descr = models.TextField(verbose_name="Описание локации", null=True, blank=True)
    loc_type = models.PositiveSmallIntegerField(verbose_name='Тип локации', choices=LOCATION_TYPE, default=1)
    room = MultiSelectField(verbose_name='Помещение', choices=ROOM_TYPE, max_choices=20, max_length=30, default=ROOM_TYPE[12][0])
    style = models.PositiveSmallIntegerField(verbose_name='Стиль', default=1, choices=STYLE_TYPE)
    addres = models.PositiveSmallIntegerField(verbose_name='Местоположение', default=0, choices=LOCATION_PLACE)
    address_custom = models.CharField(max_length=200, verbose_name=u"Местоположение кастом", null=True, blank=True, help_text='Если заполнено то учитывается только оно')
    alias = models.SlugField(max_length=200, verbose_name=u"Url", unique=True)
    
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        try:
            if not [i for i in os.listdir(os.getcwd() + '/media/locations') if re.search('^{}_.*'.format(self.pk), i) != None]:
                os.mkdir(os.getcwd() + '/media/locations/{}_{}'.format(self.pk, self.alias))
        except:
            pass
            
    def delete(self, *args, **kwargs):
        shutil.rmtree(os.getcwd() + '/media/locations/{}_{}'.format(self.pk, self.alias))
        super().delete(*args, **kwargs)
        
    
    def get_first_item(self):
        try:
            return self.location_photos.all().order_by('ordering')[0]
        except:
            None

    def get_absolute_url(self):
        return reverse('main:card', args=[self.alias])

    def __str__(self):
        return self.name
        
class LocationPhoto(models.Model):
    class Meta:
        verbose_name = u"Фотографии локаций"
        verbose_name_plural = u"Фотография локации"
        ordering = ['ordering']
        
    image = models.ImageField(verbose_name=u"Фото", upload_to='locations')
    name = models.CharField(max_length=200, verbose_name=u"Название", null=True, blank=True)
    ordering = models.IntegerField(verbose_name=u"Порядок", default=200)
    location = models.ForeignKey(Location, verbose_name=u"Локация", related_name="location_photos", on_delete=models.CASCADE)
    
    def delete(self, *args, **kwargs):
        os.remove('/home/pweb/location' + self.image.url)
        super().delete(*args, **kwargs)
    
    def __str__(self):
        return self.location.name

class Member(models.Model):
    class Meta:
        verbose_name = u"Участники команды"
        verbose_name_plural = u"Участник команды"

    name = models.CharField(max_length=200, verbose_name=u"Название")
    image = models.ImageField(verbose_name=u"Фото")
    phone = models.CharField(max_length=200, verbose_name=u"Телефон")
    email = models.CharField(max_length=200, verbose_name=u"Емайл")

    def __str__(self):
        return self.name

class Session(models.Model):

    class Meta:
        verbose_name = u"Сессии"
        verbose_name_plural = u"Сессия"

    token = models.UUIDField(default=uuid.uuid4, editable=False)
    favourite = JSONField(verbose_name=u"Избранное", default=list, null=True, blank=True)

    @classmethod
    def set_session(cls, request):
        # Инициализация сессии пользователя
        cart = cls.objects.filter(token=request.session.get(django_settings.CART_SESSION_ID)).first()
        if not cart:
            # Сохраняем сессию
            cart = cls.objects.create(token=str(uuid.uuid4()))
            request.session[django_settings.CART_SESSION_ID] = cart.token

        return cart

    get_session = set_session

    def __str__(self):
        return self.token
        
class Partners(Page):
    class Meta:
        verbose_name = u"Партнер"
        verbose_name_plural = u"Партнеры"
        
    image = models.ImageField(verbose_name=u"Фото", upload_to='partners')
    name = models.CharField(max_length=200, verbose_name=u"Название", null=True, blank=True)
    url = models.CharField(max_length=200, verbose_name=u"Url партнера", null=True, blank=True)

    def __str__(self):
        return self.name


class Service(models.Model):
    class Meta:
        verbose_name = u"Услуга"
        verbose_name_plural = u"Услуги"

    name = models.CharField(max_length=200, verbose_name=u"Название")
    description = models.TextField(verbose_name="Описание", null=True, blank=True)

    def __str__(self):
        return self.name


class Popular(models.Model):
    class Meta:
        verbose_name = u"Популярный запрос"
        verbose_name_plural = u"Популярные запросы"

    name = models.CharField(max_length=200, verbose_name=u"Название")
    url = models.CharField(max_length=200, verbose_name=u"Url партнера", help_text="https://locationbook.ru/location-base/?room=1")

    def __str__(self):
        return self.name

class TextVariable(models.Model):
    class Meta:
        verbose_name = u"Текстовая переменная"
        verbose_name_plural = u"Текстовые переменные"

    name = models.CharField(max_length=200, verbose_name=u"Название", help_text='Чтобы понять где находится')
    description = models.TextField(verbose_name=" Текст", null=True, blank=True)

    def __str__(self):
        return self.name