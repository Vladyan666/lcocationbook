from django.conf.urls import url, include
from django.conf.urls import handler404, handler500
from django.conf import settings
from django.conf.urls.static import static

from . import views

app_name = 'main'


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^location-base/$', views.base, name='base'),
    url(r'^update_photos_list/$', views.update_photos, name='update_photos_list'),
    url(r'^blog/$', views.blog, name='blog'),
    url(r'^fav/$', views.favourite, name='fav'),
    url(r'^blog/(?P<alias>[a-zA-Z0-9\-\_]+)/$', views.blog_item, name='blog_item'),
    url(r'^location-base/(?P<alias>[a-zA-Z0-9\-\_]+)/$', views.card, name='card'),
    url(r'^ajax/$', views.ajax, name='ajax'),
    url(r'^robots\.txt$', views.robots, name='robots'),
    url(r'^(?P<alias>[0-9A-Za-z\-]+)/$', views.textpage, name='textpage'),
    url(r'^get_fields/$', views.get_fields, name='get_fields'),
    url(r'^sitemap\.xml/$', views.sitemap_xml, name='sitemap'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
