def imageResize(data, output_size):
    """
    Resize image for thumbnails and preview
    data — image for resize
    output_size — turple, contains width and height of output image, for example (200, 500)
    """
    
    from PIL import Image
    
    image = Image.open(data)
    m_width = float(output_size[0])
    m_height = float(output_size[1])
    
    if image.mode not in ('L', 'RGB'):
        image = image.convert('RGB')
        
    w_k = image.size[0]/m_width
    h_k = image.size[1]/m_height
    
    if output_size < image.size:
        if w_k > h_k:
            new_size = (m_width, image.size[1]/w_k)
        else:
            new_size = (image.size[0]/h_k, m_height)  
    else:
        new_size = image.size
    return image.resize(new_size,Image.ANTIALIAS)
    
def make_upload_path(instance, filename, prefix = False):
    # Переопределение имени загружаемого файла.
    from utils.hashfunc import get_hash
    filename = 'a_' + get_hash('md5') + '.jpg'
    return u"%s/%s" % (settings.AVATAR_UPLOAD_DIR, filename)