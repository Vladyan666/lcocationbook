import django
from itertools import chain
from django.shortcuts import render
from django.http import HttpResponse
from django.http.response import Http404
from django.template import RequestContext, loader
from django.template.context_processors import csrf
from django.contrib.auth.decorators import user_passes_test
from django.http.response import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.admin.models import LogEntry
from django.contrib import messages
from django.db.models import Q
from django.conf import settings as django_settings
from django.contrib.sitemaps import Sitemap
from django.contrib.sitemaps.views import x_robots_tag
from django.core.mail import send_mail, BadHeaderError
import json
import requests

from .models import *
from .forms import ContactForm

_MAX_SIZE = 1024

def default_context(request, alias, object):
    try:
        data = object.objects.get(alias=alias)
    except:
        raise Http404

    session = Session.get_session(request=request)

    variables = TextVariable.objects.all()

    c = {}
    c.update(csrf(request))
    
    context_object = {
        'session': session,
        'data': data,
        'c': c,
        'rooms_type': ROOM_TYPE,
        'locations_type': LOCATION_TYPE,
        'styles_type': STYLE_TYPE,
        'siteinfo': django_settings.SITESETTINGS,
        'variables': variables,
    }
    
    return context_object

def index(request):
    
    template = loader.get_template('index.html')

    context_data = default_context(request, 'index', TextPage)

    if request.is_ajax():
        if request.POST['type'] == 'forma':
            _default = 'Не указано'
            name = request.POST.get('params[name]', _default)
            phone = request.POST.get('params[phone]', _default)
            email = request.POST.get('params[email]', _default)
            project = request.POST.get('params[project]', _default)
            days = request.POST.get('params[days]', _default)
            loc_type = request.POST.get('params[loc_type]', _default)
            subject = u'Заказ на сайте locationbook'
            messaga = u'Инфо о заказе:<br>Имя: {}<br>Телефон: {}<br>Почта: {}<br>Тип локации: {}<br>Кол-во съемочных дней: {}<br>Проект: {}'.format(
                name, phone, email, loc_type, days, project)
            messaga2 = u'Инфо о заказе:\r\nИмя: {}\r\nТелефон: {}\r\nПочта: {}\r\nЛокация: {}\r\nПроект: {}'.format(
                name, phone, email, loc_type, project)
            recipients = [django_settings.EMAIL_HOST_USER_RECIP]

            send_mail(
                subject, messaga, django_settings.EMAIL_HOST_USER, recipients, html_message=messaga,
                fail_silently=not getattr(django_settings, 'DEBUG', False)
            )
            r = requests.get('https://api.telegram.org/bot870234410:AAEOYxU42AZOdMXvbzBkEPt4n8NZmBkiYJU/sendMessage?chat_id=-388954882&text={}'.format(messaga2), timeout=2)

        return HttpResponse('good')
    else:
        locations = Location.objects.filter(to_main=True).prefetch_related('location_photos').order_by('-id')
        last_locations = Location.objects.all().prefetch_related('location_photos').order_by('-id')[:10]
        services = Service.objects.all()
        partners = Partners.objects.all()
        members = Member.objects.all()
        blog_pages = BlogPage.objects.all().order_by('-datetime')[:4]
        page_form = ContactForm()
        popular = Popular.objects.all()
    
    context_data.update({
        'page_form': page_form,
        'last_locations': last_locations,
        'locations': locations,
        'members': members,
        'partners': partners,
        'blog_pages': blog_pages,
        'services': services,
        'popular': popular,
    })
    
    return HttpResponse(template.render(context_data))
    
def base(request):

    template = loader.get_template('search.html')

    context_data = default_context(request, 'location-base', TextPage)

    filters = {}
    str_url = []
    rooms = None


    if request.GET.get('location_type'):
        filters.update({'loc_type__in': [i for i in request.GET.get('location_type').split(',')]})
        str_url.append('location_type=' + request.GET.get('location_type'))

    if request.GET.get('room'):
        rooms = Q()
        for i in request.GET.get('room').split(','):
            rooms |= Q(room__icontains=i)

    if request.GET.get('style'):
        filters.update({'style__in': [i for i in request.GET.get('style').split(',')]})
        str_url.append('style=' + request.GET.get('style'))

    locations = Location.objects.filter(**filters).prefetch_related('location_photos')

    if rooms:
        locations = locations.filter(rooms)

    paginator = Paginator(locations, 12)
    page = request.GET.get('page')

    if str_url:
        dop_url = '&' + '&'.join(str_url)
    else:
        dop_url = ''

    try:
        locs = paginator.page(page)
    except PageNotAnInteger:
        locs = paginator.page(1)
    except EmptyPage:
        locs = paginator.page(paginator.num_pages)

    context_data.update({
        'dop_url': dop_url,
        'locs': locs,
        'locations': locations,
    })

    return HttpResponse(template.render(context_data))

def favourite(request):
    template = loader.get_template('fav.html')

    context_data = default_context(request, 'location-base', TextPage)

    locations = Location.objects.filter(pk__in=context_data['session'].favourite)

    context_data.update({
        'locations': locations,
    })

    return HttpResponse(template.render(context_data))
    
def card(request, alias):
    
    template = loader.get_template('card.html')

    context_data = default_context(request, alias, Location)
        
    return HttpResponse(template.render(context_data))
    
def blog(request):
    
    template = loader.get_template('blog.html')

    context_data = default_context(request, 'blog', TextPage)
    
    blog_items = BlogPage.objects.all()
    
    context_data.update({
        'blog_items': blog_items,
    })
        
    return HttpResponse(template.render(context_data))


def ajax(request):
    cart = Session.get_session(request=request)
    if request.is_ajax():
        if request.POST['type'] == 'add_fav':
            cart.favourite.append(int(request.POST['id']))
            cart.save()
            val = str(cart.favourite)
        elif request.POST['type'] == 'remove_fav':
            cart.favourite = list(set(cart.favourite) ^ set([int(request.POST['id'])]))
            cart.save()
            val = str(cart.favourite)
        elif request.POST['type'] == 'book_room':
            _default = 'Не указано'
            name = request.POST.get('params[name]', _default)
            phone = request.POST.get('params[phone]', _default)
            email = request.POST.get('params[email]', _default)
            project = request.POST.get('params[project]', _default)
            location = request.POST.get('params[location]', _default)

            subject = u'Заказ на сайте locationbook'
            messaga = u'Инфо о заказе:<br>Имя: {}<br>Телефон: {}<br>Почта: {}<br>Локация: {}<br>Проект: {}'.format(name, phone, email, location, project)
            messaga2 = u'Инфо о заказе:\r\nИмя: {}\r\nТелефон: {}\r\nПочта: {}\r\nЛокация: {}\r\nПроект: {}'.format(name, phone, email, location, project)
            recipients = [django_settings.EMAIL_HOST_USER_RECIP]

            send_mail(
                subject, messaga, django_settings.EMAIL_HOST_USER, recipients, html_message=messaga,
                fail_silently=not getattr(django_settings, 'DEBUG', False)
            )
            r = requests.get('https://api.telegram.org/bot870234410:AAEOYxU42AZOdMXvbzBkEPt4n8NZmBkiYJU/sendMessage?chat_id=-388954882&text={}'.format(messaga2), timeout=2)
            val = 'good'

    return HttpResponse(val)
    
def blog_item(request, alias):
    
    template = loader.get_template('textpage.html')

    context_data = default_context(request, alias, BlogPage)
        
    return HttpResponse(template.render(context_data))

def textpage(request, alias):

    template = loader.get_template('textpage.html')

    context_data = default_context(request, alias, TextPage)

    return HttpResponse(template.render(context_data))

def update_photos_list():
    already_exists = list(LocationPhoto.objects.all().values_list('image', flat=True))
    
    all_files = []
    
    for root, dirs, files in os.walk(os.getcwd() + '/media/locations/', topdown = False):
        all_files += [os.path.join(root, name).split('/media/')[1] for name in files]
        
    result = list(set(all_files) ^ set(already_exists))
    
    for item in result:
        location = Location.objects.get(pk=item.split('/')[1].split('_')[0])

        # Проверяем, указан ли логотип
        if item:
            image = Image.open(os.getcwd() + '/media/' + item)
            width = image.width
            height = image.height
            max_size = max(width, height)
            
            # Может, и не надо ничего менять?
            if max_size > _MAX_SIZE:
                image = image.resize((round(width / max_size * _MAX_SIZE), round(height / max_size * _MAX_SIZE)), Image.ANTIALIAS)
                image.save(os.getcwd() + '/media/' + item)
            LocationPhoto.objects.create(location=location, image=item, ordering=200)
        
    
@user_passes_test(lambda u: u.is_superuser)
def update_photos(request):
    update_photos_list()
    LogEntry.objects.create(object_repr='Обновление фотографий', action_flag=True, user=request.user)
    messages.info(request, message='Фотографии обновлены')
    return HttpResponseRedirect(reverse_lazy('admin:index'))

def get_fields(request):
    if request.is_ajax():
        q = request.GET.get('term', '')
        drugs = Location.objects.filter(Q(name__icontains=q)|Q(loc_descr__icontains=q))[:8]
        results = []
        for drug in drugs:
            drug_json = {}
            drug_json['name'] = drug.name
            drug_json['alias'] = drug.alias
            results.append(drug_json)
        data = json.dumps(results)
    else:
        data = 'fail'
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)

def robots(request):
    template = loader.get_template('robots.txt')
    context = {}

    return HttpResponse(template.render(context), content_type="text/plain")


def get_model(text):
    return django.apps.apps.get_model('main', text)

def sitemap_xml(request):
    template = loader.get_template('sitemap_xml.html')


    # chain - делает из списка списков - 1 большой список
    sitemap_sections = list(
        chain.from_iterable([[s for s in get_model(i).objects.all()] for i in ['TextPage', 'Location']]))

    host = 'https://{}'.format(request.get_host())

    urls = ['{}{}'.format(host, i.get_absolute_url()) for i in sitemap_sections]

    context = {
        'urls': urls,
    }

    return HttpResponse(template.render(context), content_type='application/xhtml+xml')