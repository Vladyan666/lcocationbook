$(document).ready(function () {

        var can_click = true;

        // Слайдер на главной
        if ($('.main_slider').length) {
            $('.main_slider__count_2').html($('.main_slider__slide').length);

            $('.main_slider').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                prevArrow: '<div class="main_slider__nav main_slider__nav-prev"></div>',
                nextArrow: '<div class="main_slider__nav main_slider__nav-next"></div>',
            });

            $(".main_slider").on('afterChange', function(event, slick, currentSlide, nextSlide){
                $('.main_slider__count_1').html(currentSlide + 1);
            });
        }

        // Слайдер на локаций
        if ($('.location__wrp').length) {
            $('.location__wrp').slick({
                slidesToShow: 4,
                slidesToScroll: 2,
                prevArrow: '<div class="location__nav location__nav-prev"></div>',
                nextArrow: '<div class="location__nav location__nav-next">Следующие →</div>',
                appendArrows : '.location__next',
                responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    },
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        centerMode: true
                    }
                }]
            });
        }

        // Слайдер в карточке
        if ($('.card__w').length) {
            $('.card__w').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                prevArrow: '<div class="card__slider__nav card__slider__nav-prev">←</div>',
                nextArrow: '<div class="card__slider__nav card__slider__nav-next">→</div>'
            });
        }

        // Слайдер плитки в карточке
        if ($('.card__tile').length) {
            $('.card__tile').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                prevArrow: '<div class="card__tile__nav card__tile__nav-prev">←</div>',
                nextArrow: '<div class="card__tile__nav card__tile__nav-next">→</div>'
            });

            
            $(".card__tile").on('beforeChange', function(event, slick, currentSlide, nextSlide){
                can_click = false;
            });

            $(".card__tile").on('afterChange', function(event, slick, currentSlide, nextSlide){
                can_click = true;
            });

            card__tile_active(true);
        }

        // Открыть фильтр
        $(".main_filter__title").on("click", function(){
            $(this).closest(".main_filter").toggleClass("active");
        });

        // Открыть / закрыть услугу
        $(".main_block__line").on("click", function(){
            $(this).toggleClass("active");
        });

        // Открыть меню
        $(".header__menu").on("click", function(){
            $(".header_drop").toggleClass("active");
            $(".header .filter, .header_search").removeClass("active");
        });

        // Закрыть меню
        $(".close-drop").on("click", function(){
            $(".header_drop").removeClass("active");
        });

        // Показать фильтр на главной
        $(".main_filter__title").on("click", function(){
            $(".main_filter .filter").toggleClass("active");
        });

        // Показать фильтр в шапке
        $(".header__filter").on("click", function(){
            $(".header .filter").toggleClass("active");
            $(".header__menu, .header_drop, .header_search").removeClass("active");
        });

        // Скрыть фильтр
        $(".close-filter").on("click", function(){
            $(this).closest(".filter").removeClass("active");
        });

        // Показать поиск в шапке
        $(".header__search").on("click", function(){
            $(".header_search").toggleClass("active");
            $(".header__menu, .header_drop, .header .filter").removeClass("active");
        });

        // Скрыть поиск
        $(".close-search").on("click", function(){
            $(".header_search").removeClass("active");
        });

        // Показать отправку запроса
        $(".card__button-show").on("click", function(){
            $(".card__wrp").removeClass("active");
            $(".card__wrp-2").addClass("active");
        });

        // Скрыть отправку запроса
        $(".close-card").on("click", function(){
            $(".card__wrp").removeClass("active");
            $(".card__wrp-1").addClass("active");
        });

        // Показать плитку в карточке
        $(".card__all").on("click", function(){
            $(".card__slider, .card__ul li").removeClass("active");
            $(".card__tile").addClass("active");
        });

        // Показать слайдер в карточке
        $(".card__ul li, .card__img").on("click", function(){
            if (can_click) {
                var id = $(this).index() ;
                $('.card__w').slick('slickGoTo', id);
    
                $(".card__tile, .card__ul li").removeClass("active");
                $(".card__ul li:nth-child(" + parseInt(id + 1) + ")").addClass("active");
                $(".card__slider, .card__gal").addClass("active");

                card__tile_active(false);
            }
        });

        // Моб. переключение фильтров
        $(".filter__title").on("click", function(){
            id = $(this).closest(".filter__col").index();

            $(this).closest(".filter").find(".filter__title, .filter__col-col").removeClass("active");
            $(this).closest(".filter").find(".filter__col-col:nth-child(" + id + ")").addClass("active");
            
            $(this).addClass("active");
        });

        // Моб. переключение в карточке
        $(".card_nav__nav").on("click", function(){
            id = $(this).index() + 1;

            $(".card_nav__nav, .card__show").removeClass("active");
            $(".card__show-" + id).addClass("active");
            
            $(this).addClass("active");
        });

        // Показать галлерею
        $(".card__gal").on("click", function(){
            $(this).toggleClass("active");
            $(".card__slider").toggleClass("active");
        });   

        // Скрыть галлерею
        $(".close-card__slider").on("click", function(){
            $(".card__slider, .card__gal").removeClass("active");

            card__tile_active(true);
        });   

        // fancybox
        if ($(".fancybox").length) {
            $(".fancybox").fancybox();
        }

        function card__tile_active(slider) {
            if (slider) {
                var $carousel = $('.card__tile');
            } else {
                var $carousel = $('.card__w');
            }

            $(document).on('keydown', function(e) {
                if(e.keyCode == 37) {
                    $carousel.slick('slickPrev');
                }
                if(e.keyCode == 39) {
                    $carousel.slick('slickNext');
                }
            });
        }

});
